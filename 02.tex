\chapter{Il progetto di circuiti logici}
Per fare un circuito con determinate \textbf{specifiche} (costo, prestazioni\textellipsis ), si scelgono dei componenti, avendo informazioni di base sulla loro funzionalità, e li si assembla (\textbf{topologia}).

I \textbf{sistemi digitali} elaborano input digitali in output digitali (= binari).

Il circuito dispone di una libreria di componenti che rappresenta le \textbf{porte logiche} da interconnettere, rappresentabili con un grafo con:
\begin{itemize}
\item vertici = porte logiche
\item archi = linee per il trasferimento dei dati tra le porte
\end{itemize}

Un circuito è definito da:
\begin{itemize}
\item \textbf{struttura:} da quali componenti è composto il circuito e come sono connessi questi componenti
\item \textbf{comportamento:} ciò che fa
\end{itemize}

Due circuiti con strutture diverse possono avere lo stesso comportamento; il viceversa è da evitare.

\section{Progettazione}
Dati un comportamento e una struttura, il progettista deve interconnettere i componenti tenendo conto del comportamento e del costo.

Il \textbf{costo di progetto}/sviluppo si paga una sola volta, ma è significativo $\Rightarrow$ deve essere recuperato sul \textbf{costo di produzione} dei componenti (vivo), che deve essere relativamente basso. Il prodotto deve richiedere un \textbf{costo di manutenzione} ragionevole $\Rightarrow$ meno guasti possibile e costi di riparazione bassi.

\paragraph{Flusso di progetto} specifiche $\rightarrow$ sintesi $\rightarrow$ verifica $\rightarrow$ realizzazione

Il progettista di un circuito realizza prima un \textbf{modello}, che descrive le porte logiche e le loro interconnessioni che soddisfano le specifiche, quindi verifica tramite simulazione che si comporti come previsto prima di metterlo in produzione.

Il costo di progettazione a volte viene fatto in maniera top-down, partendo dalle specifiche e realizzando un progetto suddividendo il problema in sottoproblemi più semplici, oppure partendo da un modello simile già esistente e modificando alcune parti.

\subsection{Livelli di progettazione}
Il progetto può avvenire a diversi livelli. Un approccio top-down prevede la discesa nei seguenti livelli:
\begin{itemize}
\item \textbf{sistema:} opera su blocchi di dati codificati in un certo modo (es. output allo schermo), e utilizza dei componenti già noti;
\item \textbf{registri:} non opera su 0 e 1, ma sulle variabili (ad es. si progetta il circuito per il nuovo standard USB partendo da blocchi logici elementari e assemblandoli) (si veda la sezione~\ref{sez:livello_registri});
\item \textbf{porte logiche:} gli elementi assemblati dal progettista sono le porte logiche, in grado di produrre dei segnali di uscita su ciascuna porta logica che dipendono dai segnali in ingresso e che sono determinati da funzioni booleane implementate in hardware (si veda la sezione~\ref{sez:livello_porte});
\item \textbf{transistor:} livello molto basso, ma sempre su grandezze digitali;
\item \textbf{elettrico:} il circuito è descritto dal progettista a livello elettrico, cioè in termini di connessioni di componenti quali condensatori, induttori, ecc. $\Rightarrow$ si riduce allo studio di un sistema di equazioni differenziali in cui compaiono correnti e tensioni.
\end{itemize}

Più si scende di livello, più il dettaglio di \textbf{tempo} è importante, e più piccoli e semplici devono essere i circuiti per poter essere studiati.

\section{Livello di porte logiche}
\label{sez:livello_porte}
Il progetto di un sistema sequenziale sincrono si compone delle seguenti fasi:\footnote{Si vedano le sezioni~\ref{sez:rappresentazione} e~\ref{sez:sistemi_sequenziali}.}
\begin{enumerate}
\item si costruiscono il diagramma degli stati e la tavola degli stati;
\item \ul{minimizzazione degli stati:} un algoritmo cerca di minimizzare il diagramma degli stati, ovvero di ricondurlo a un diagramma equivalente\footnote{Due diagrammi di stato si dicono \textbf{equivalenti} se a parità di sequenze di ingresso, il comportamento non cambia.} riunendo più stati in uno;
\item \ul{assegnazione degli stati:} data una serie di stati con nomi simbolici, si assegna a ciascuno stato una codifica binaria e si calcola il numero di flip-flop necessari per memorizzarli:
\[
 \sup \{ \log_2 {( \text{numero degli stati} )} \}
\]
\item \ul{costruzione della tavola di verità della rete combinatoria:} per ogni combinazione di ingressi e di stati correnti, si elencano le uscite e gli stati futuri;
\item \ul{sintesi della rete combinatoria:} tramite le tavole di Karnaugh, si esprimono le funzioni $f$ e $h$ (ciascun bit è dato da una funzione booleana);
\item si disegna il circuito.
\end{enumerate}

\subsection{Circuiti combinatori}
I \textbf{circuiti combinatori} implementano funzioni combinatorie. Una \textbf{funzione combinatoria} è una trasformazione:
\[
 z : B^n \to B
\]
dove $B = \{ 0,1 \}$.

In un sistema combinatorio, i valori di uscita dipendono esclusivamente dai valori applicati ai suoi ingressi in quell'istante.

\subsubsection{Rappresentazione}
\label{sez:rappresentazione}
Il sistema deve trasformare un certo numero di bit in ingresso in un certo numero di bit in uscita. Il comportamento di un sistema combinatorio può essere descritto da:
\begin{itemize}
\item \textbf{funzione booleana:} es. $f(a,b)=a \cdot b$ con $\cdot$ = porta AND;
\item \textbf{tavola di verità:} la tabella indica l'output per ogni combinazione in ingresso.
\end{itemize}

\paragraph{Esempio}
Il codificatore prioritario indica l'indice del primo bit in ingresso di valore 1. Si può descrivere con funzioni booleane o più semplicemente con una tavola di verità.

\subsubsection{Terminologia}
\label{sez:terminologia}
\begin{table}[H]
  \centering
  \begin{tabular}{|c|c|c|c}
    \cline{1-3}
    %\textbf{AND} & \textbf{OR} & \textbf{XOR} & \multirow{6}{*}{\multicolumn{1}{@{}c@{}}{\begin{tabular}{c|}
    %\hline
    %\textbf{NOT} \\
    %\hline
    %\includegraphics[width=0.15\linewidth]{pic/02/NOT_ANSI} \\
    %$\displaystyle \overline{x} $ \\
    %\hline
    %\end{tabular}}} \\
    \textbf{AND} & \textbf{OR} & \textbf{XOR} & \multicolumn{1}{@{}c@{}}{\multirow{10}{*}{\begin{tabular}{c|}
    \hline
    \textbf{NOT} \\
    \hline
    \includegraphics[width=0.15\linewidth]{pic/02/NOT_ANSI} \\
    $\displaystyle \overline{x} $ \\
    \hline
    \end{tabular}}} \\
    \cline{1-3}
    \includegraphics[width=0.15\linewidth]{pic/02/AND_ANSI} & \includegraphics[width=0.15\linewidth]{pic/02/OR_ANSI} & \includegraphics[width=0.15\linewidth]{pic/02/XOR_ANSI} & \\
    $\displaystyle x_1 \cdot x_2$ & $\displaystyle x_1+x_2$ & $\displaystyle x_1 \oplus x_2$ & \\
    \cline{1-3}
    \textbf{NAND} & \textbf{NOR} & \textbf{XNOR} & \\
    \cline{1-3}
    \includegraphics[width=0.15\linewidth]{pic/02/NAND_ANSI} & \includegraphics[width=0.15\linewidth]{pic/02/NOR_ANSI} & \includegraphics[width=0.15\linewidth]{pic/02/XNOR_ANSI} & \\
    $\displaystyle \overline{x_1 \cdot x_2}$ & $\displaystyle \overline {x_1+x_2}$ & $\displaystyle \overline{x_1 \oplus x_2}$ & \\
    \cline{1-3}
  \end{tabular}
  \caption[]{Porte logiche.\footnotemark}
\end{table}
\footnotetext{Questa tabella contiene immagini che sono tratte da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:AND_ANSI.svg}{AND ANSI.svg}, \href{https://commons.wikimedia.org/wiki/File:OR_ANSI.svg}{OR ANSI.svg}, \href{https://commons.wikimedia.org/wiki/File:XOR_ANSI.svg}{XOR ANSI.svg}, \href{https://commons.wikimedia.org/wiki/File:NAND_ANSI.svg}{NAND ANSI.svg}, \href{https://commons.wikimedia.org/wiki/File:NOR_ANSI.svg}{NOR ANSI.svg}, \href{https://commons.wikimedia.org/wiki/File:XNOR_ANSI.svg}{XNOR ANSI.svg}, \href{https://commons.wikimedia.org/wiki/File:NOT_ANSI.svg}{NOT ANSI.svg}), sono state realizzate dall'utente \href{https://commons.wikimedia.org/wiki/User:Jjbeard}{jjbeard} e si trovano nel dominio pubblico.}

\begin{description}
 \item[fan-in] numero di ingressi di una porta
 \item[fan-out] numero di altre porte pilotate dall'uscita di una porta
\end{description}

Due porte logiche sono in connessione \textbf{wired-or} se le loro uscite vanno agli ingressi di una porta OR, che si può anche omettere.

\begin{table}[H]
 \centering
 \begin{tabular}{|c|c|c|c|}
  \hline
  \textbf{letterale} & \textbf{minterm} & \textbf{maxterm} & \textbf{cubo} \\
  \hline
$\displaystyle \dot{x_i}$ & $\displaystyle \dot{x_1} \cdot \dot{x_2} \cdot \cdots \cdot \dot{x_n} $ & $\displaystyle \dot{x_1} + \dot{x_2} + \cdots + \dot{x_n} $ & $\displaystyle \dot{x_i} \cdot \cdots \cdot \dot{x_j} \cdot \cdots \cdot \dot{x_k}$ \\
  \hline
 \end{tabular}
 \caption[]{\footnotemark}
\end{table}
\footnotetext{Il puntino su una variabile indica che la variabile può comparire affermata o negata.}

\subsubsection{Circuiti combinatori ben formati}
I \textbf{circuiti combinatori ben formati} (ccbf) sono caratterizzati da una delle seguenti composizioni:
\begin{itemize}
\item una singola linea;
\item una singola porta;
\item due ccbf giustapposti (cioè uno accanto all'altro);
\item due ccbf tali che le uscite di uno sono gli ingressi dell'altro;
\item due ccbf tali che uno degli ingressi di uno è anche uno degli ingressi dell'altro.
\end{itemize}

Sono vietati i circuiti dotati di cicli.

\paragraph{Circuiti a due livelli}
\begin{itemize}
\item \ul{somma di prodotti:} le uscite di diverse porte and sono pilotate da un'unica porta or;
\item \ul{prodotto di somme:} le uscite di diverse porte or sono pilotate da un'unica porta and.
\end{itemize}

\subsubsection{Minimizzazione}
Data una tavola di verità, ci possono essere più \textbf{circuiti equivalenti}/funzioni che la implementano.

È possibile costruire lo schema di un circuito dalla sua tavola di verità posizionando una porta OR avente per ingressi le uscite delle porte AND che danno 1, ma il circuito risultante non è quello minimo.

\paragraph{Circuiti minimi}
Con l'\textbf{algebra booleana} si può semplificare una funzione booleana in un circuito minimo (operazione di \textbf{minimizzazione}).

Il problema di trovare un \textbf{circuito minimo} è di tipo NP-completo. Un circuito minimo è composto dal minimo numero possibile di porte logiche (al più 2 livelli). Implementa una somma di prodotti tale che il numero di prodotti è minimo (cubo), e nessun letterale può essere cancellato da un prodotto.

Dato un numero di variabili in ingresso ragionevole, il circuito minimo è ricavato dalle \textbf{tavole di Karnaugh}, che esprimono il comportamento di un circuito nella maniera più compatta possibile, cercando la copertura degli uni con il minimo numero di cubi.

\paragraph{Valori don't care}
\label{sez:dont_care}
I \textbf{valori don't care} sono quelli che non sono forniti dalla specifica nella tavola di verità, perché le relative combinazioni in ingresso non si verificano mai.

Nella \textbf{codifica BCD}, si assegnano 4 bit (4 ingressi) a ciascuna delle cifre decimali $\Rightarrow$ siccome le cifre vanno da 1 a 9, avanzano dei bit che sono i valori don't care.

\subsubsection{Ritardi}
Una porta logica è composta da transistor, rappresentabili come degli interruttori che si chiudono quando la tensione applicata al segnale di controllo è alta (1 = cortocircuito) e viceversa (0 = circuito aperto). Una opportuna combinazione di transistor può far commutare la tensione di uscita $V_\text{out}$ nei valori:
\begin{itemize}
\item 1: $V_\text{out}$ viene lasciata essere uguale alla tensione di alimentazione $V_\text{cc}$, a cui è collegata attraverso la \textbf{resistenza pull-up}\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Resistenza_pull-up}{Resistenza pull-up} su Wikipedia in italiano.};
\item 0: $V_\text{out}$ risulta collegata alla massa.
\end{itemize}

Ogni transistor ha un suo tempo di commutazione $\Rightarrow$ il cambiamento di un valore in ingresso si riflette con un certo \textbf{ritardo} nel valore di uscita $\Rightarrow$ il valore in ingresso non viene modificato prima che sia passato il tempo $t_0 + k$ per la commutazione del valore in uscita.

Il \textbf{cammino critico} è il cammino di lunghezza massima che va da un ingresso a un'uscita, e si può trovare assegnando un livello a ciascuna porta. Come requisito per il circuito minimo si può stabilire una profondità massima. La \textbf{profondità} massima di un circuito è la lunghezza del cammino critico, ovvero il numero di porte logiche che costituiscono il cammino critico.

La profondità è legata alla velocità del circuito, cioè al numero massimo di configurazioni che è in grado di gestire nell'unità di tempo. Il ritardo massimo è un numero di unità di tempo pari alla profondità massima.

\subsection{Sistemi sequenziali}
\label{sez:sistemi_sequenziali}
In un \textbf{sistema sequenziale}, il valore di uscita dipende anche da ciò che è successo negli istanti precedenti.

Il numero di storie precedenti non può essere infinito, perché il numero di bit utilizzabili per memorizzare la storia precedente è finito. Una variabile interna $Y$ detta \textbf{variabile di stato}, codificata in un certo numero di bit, memorizza un valore numerico che corrisponde a una determinata storia. L'output viene determinato in base agli ingressi correnti $X$ e alla variabile di stato $Y$.

\paragraph{Esempio}
Un sistema campiona l'ingresso $I$ con una certa frequenza di tempo. L'uscita $O$ assume il valore 1 se durante i 3 istanti di campionamento precedenti l'ingresso $I$ ha assunto i valori 101, e assume il valore 0 diversamente.

\subsubsection{Rappresentazione}
\paragraph{Diagramma degli stati}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/02/Diagramma_stati.png}
	\caption{Esempio di diagramma degli stati.}
\end{figure}

\noindent
Il \textbf{diagramma degli stati} si basa su una scala dei tempi discretizzata in punti ben definiti:
\begin{itemize}
\item \ul{stato corrente:} la variabile di stato può assumere i valori rappresentati come lettere cerchiate;
\item \ul{ingresso:} ogni arco rappresenta il cambio di stato, in base al valore in ingresso corrente;
\item \ul{uscita:} si può scegliere di rappresentare i valori di output in due modi:
\begin{itemize}
\item a pedice dello stato (come in figura): questo valore è un unico bit, quindi può dipendere solo dallo stato corrente e non dagli ingressi correnti;
\item direttamente sugli archi, racchiusi in un rettangolo: facilita la rappresentazione di valori di uscita composti da più bit.
\end{itemize}
\end{itemize}

Il numero totale degli archi è uguale a: numero degli stati $\times$ numero delle combinazioni di ingresso.

\paragraph{Tavola degli stati}
La \textbf{tavola degli stati} elenca tutte le combinazioni possibili di stati correnti e valori in ingresso. Il numero delle righe si calcola: numero degli stati $\times$ 2\textsuperscript{numero di bit in ingresso} $\Rightarrow$ diventa troppo lunga in caso di alti numeri di stati e di bit in ingresso.

\paragraph{Funzioni di transizione e d'uscita degli stati}
La \textbf{funzione di transizione} e la \textbf{funzione di uscita} sono delle funzioni booleane che, dati i valori assunti dagli ingressi correnti e dalla variabile di stato corrente\footnote{I valori assunti dalla variabile di stato sono rappresentati in formato binario anziché in lettere.}, restituiscono rispettivamente il valore futuro della variabile di stato e il valore di uscita.
\FloatBarrier

\subsubsection{Implementazione}
Il \textbf{flip-flop} è un elemento di memoria in grado di memorizzare un singolo bit.

\paragraph{Flip-flop asincroni}
\label{sez:flip_flop_asincroni}
Il \textbf{flip-flop set-reset (\textit{SR}) asincrono} è un elemento di memoria di tipo sequenziale in cui si può forzare o mantenere un certo valore binario a seconda della scelta dei due ingressi $S$ e $R$:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.35\linewidth]{pic/02/Flip-flop_SR.png}
	\caption{Flip-flop \textit{SR} asincrono.}
\end{figure}

\begin{table}[H]
 \centering
 \centerline{\begin{tabularx}{\textwidth}{|c|c|X|}
  \hline
  \textbf{ingresso $S$} & \textbf{ingresso $R$} & \multicolumn{1}{c|}{\textbf{uscita $Y_1$ e $Y_2$ = NOT $Y_1$}} \\
  \hline
  1 & 0 & Vengono forzati i valori 1 sull'uscita $Y_1$ e 0 sull'uscita $Y_2$. \\
  \hline
  0 & 1 & Vengono forzati i valori 0 sull'uscita $Y_1$ e 1 sull'uscita $Y_2$. \\
  \hline
  \multirow{2}{*}{0} & \multirow{2}{*}{0} & I valori in uscita non vengono commutati ma si mantengono costanti. \\
  \hline
  \multirow{10}{*}{1} & \multirow{10}{*}{1} & È una configurazione vietata perché la transizione dallo stato $S = R = 1$ allo stato $S = R = 0$ produce un risultato logicamente non definito: l'uscita $Y_1$ può assumere 0 o 1 a seconda dei ritardi di commutazione $\Delta_1$ e $\Delta_2$ delle due porte logiche, i quali dipendono da fattori non prevedibili (per es. temperatura, umidità\textellipsis ):
  \begin{itemize}
   \item l'uscita $Y_1$ rimane al valore 0 se $\Delta_1 < \Delta_2$, cioè la porta 2 commuta prima che la porta 1 abbia finito di commutare;
   \item l'uscita $Y_1$ commuta al valore 1 se $\Delta_1 > \Delta_2$, cioè la porta 1 riesce a finire di commutare prima che la porta 2 ha commutato.
  \end{itemize}\\
  \hline
 \end{tabularx}}
\end{table}

\paragraph{Flip-flop sincroni}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/02/Flip-flop_SR_sincrono.png}
	\caption{Flip-flop \textit{SR} sincrono.}
\end{figure}

\noindent
Il \textbf{clock} è un segnale di temporizzazione a onda quadra. In un \textbf{flip-flop \textit{SR} sincrono}, il segnale di clock si duplica in due segnali posti agli ingressi di due porte AND in coppia con entrambi gli ingressi $S$ e $R$:
\begin{itemize}
\item clock = 1: il flip-flop è sensibile alle variazioni degli ingressi $S$ e $R$;
\item clock = 0: il flip-flop mantiene il suo valore senza vedere gli ingressi $S$ e $R$, lasciando così il tempo al nuovo valore di propagarsi.
\end{itemize}

Due ulteriori segnali $P$ (preset) e $C$ (clear) servono per forzare in modo asincrono il flip-flop rispettivamente a 1 o a 0.

Nel \textbf{flip-flop di tipo $D$}, $S$ e $R$ sono uniti, attraverso una porta NOT, in un unico ingresso $D$ che permette di forzare una coppia di valori $S \neq R$, che rimarrà costante fino al successivo colpo di clock. I flip-flop di tipo $D$ sono i più utilizzati perché sono semplici e poco costosi, e non presentano configurazioni vietate.

Un \textbf{flip-flop di tipo $T$} è un flip-flop di tipo $D$ senza ingressi: il segnale di clock fa invertire il valore memorizzato (l'uscita ritorna all'ingresso tramite una porta NOT).

Un sistema sequenziale si dice \textbf{sincrono} se tutti i flip-flop sono collegati allo stesso segnale di clock.
\FloatBarrier

\paragraph{Modello di Huffman}
Il \textbf{modello di Huffman} è un circuito costituito da una \textbf{rete combinatoria} e da una serie di \textbf{flip-flop} sincroni.

Tramite il modello di Huffman si può implementare un sistema sequenziale. La variabile di stato si può memorizzare in una serie di flip-flop. Gli altri blocchi combinatori ricevono in ingresso la variabile di stato e gli altri valori in ingresso.

\subparagraph{Casi particolari}
\begin{itemize}
\item \textbf{modello di Mealy:} tutte le uscite $z_i$ della rete combinatoria dipendono sia dagli ingressi $x_i$ sia dalle variabili di stato $y_i$;
\item \textbf{modello di Moore:} almeno un'uscita $z_i$ dipende solo dalle variabili di stato $y_i$ $\Rightarrow$ tali uscite rimarranno costanti fino al successivo colpo di clock.
\end{itemize}

\section{Livello dei registri}
\label{sez:livello_registri}
Non si lavora a livello di singoli bit ma a livello di parole (= vettori di bit). Il \textbf{parallelismo} di una parola è il suo numero di bit.

\subsection{Componenti generici}
\subsubsection{Porta logica operante su parole}
La \textbf{porta logica operante su parole} è uno scatolotto contenente una batteria di porte logiche elementari, avente ingresso e uscita con eguale parallelismo di $m$ bit e con eguale numero di parole.

\subsubsection{Multiplexer}
Il \textbf{multiplexer}, pilotato dall'ingresso $p$, detto \textbf{segnale di controllo}, seleziona una delle $k$ parole in ingresso portandola in uscita.

Connettendo più multiplexer semplici \textbf{in cascata} si può avere un maggior numero di parole in ingresso.

L'uso di un multiplexer è un modo veloce (ma non efficiente) per implementare il comportamento di una generica funzione booleana data: basta porre agli ingressi con parallelismo di $m = 1$ bit tutti i possibili $2^n$ valori restituiti dalla funzione, e ai segnali di controllo gli $n$ ingressi valutati dalla funzione.

\subsubsection{Decodificatore}
Il \textbf{decodificatore} è un modulo avente $2^n$ linee in uscita e $n$ linee in ingresso. Il valore in ingresso attiva una sola linea di uscita, cioè quella avente per indice lo stesso valore in ingresso.

Accanto ai normali valori in ingresso, vi è il \textbf{valore di enable} $e$ che assume 1 o 0:
\begin{itemize}
\item $e = 1$: alla linea di uscita è consentito essere attivata dalla linea in ingresso corrente;
\item $e = 0$: nessuna uscita può essere attivata, e in uscita vi è un valore predefinito indipendente dalla linea in ingresso.
\end{itemize}

Anche i decodificatori si possono connettere in cascata: si assegna la discriminazione dei bit più significativi ai decodificatori dei primi livelli e quella dei bit meno significativi ai decodificatori dei livelli sottostanti, e si forniscono come valori di enable le uscite dei decodificatori a livello superiore. Con la connessione in cascata bisogna però tenere in conto dei ritardi di commutazione dei decodificatori (durante il tempo di commutazione $\Delta t$ i segnali in uscita sono casuali nel tempo).

\subsubsection{Codificatore}
Il \textbf{codificatore} ha un comportamento opposto a quello del decodificatore: riceve una sola delle $2^k$ linee in ingresso e restituisce fino a $k$ linee in uscita, la cui parola corrisponde all'indice della linea in ingresso corrente.

Sono però vietate le combinazioni in ingresso con più di un bit 1 $\Rightarrow$ non è molto utilizzato.

\subsubsection{Codificatore prioritario}
Nel \textbf{codificatore prioritario}, a ogni linea in ingresso è associata oltre all'indice una \textbf{priorità}. L'uscita è l'indice della linea in ingresso con priorità massima tra quelle attive.

In questo modo viene ottimizzato l'impiego dei bit in ingresso.

Siccome le priorità partono da 0, bisogna distinguere il caso in cui la linea a priorità 0 è quella attiva e il caso in cui nessuna linea è attiva $\Rightarrow$ si aggiunge il bit \textbf{Input Active} (\textit{IA}) in uscita:
\begin{itemize}
\item \textit{IA} = 1: almeno una linea in ingresso è attiva;
\item \textit{IA} = 0: nessuna linea in ingresso è attiva.
\end{itemize}

\subsection{Moduli aritmetici}
\subsubsection{Sommatore combinatorio}
Il \textbf{sommatore combinatorio} è un modulo aritmetico che riceve in ingresso 2 parole di $m$ bit e ne restituisce in uscita la somma in $m+1$ bit $\Rightarrow$ non è molto utilizzato per il suo costo di progetto: anche se richiede il minimo tempo, è da progettare per ogni numero $m$.

\subsubsection{Sommatore combinatorio modulare con ripple carry adder}
Il \textbf{sommatore combinatorio modulare} si compone di una successione (= \textbf{ripple carry adder}) di \textbf{full-adder}\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Full-adder}{Full-adder} su Wikipedia in italiano.}, che sono dei sommatori combinatori in grado di gestire il riporto. Ogni full-adder riceve solo 2 bit in ingresso ($m = 1$) e il bit di riporto/\textbf{carry} $c$ proveniente dal full-adder precedente $\Rightarrow$ maggiore flessibilità $\Rightarrow$ minore costo di progetto.

Costruendo la tavola di verità, si sintetizzano le seguenti funzioni:
\[
\begin{cases} z_i = x_i \oplus y_i \oplus c_i \\
c_{i+1} = x_i y_i + x_i c_i + y_i c_i \end{cases}
\]

I full-adder sono implementati con circuiti combinatori a $k$ livelli $\Rightarrow$ il ritardo temporale $\Delta$ di ciascun livello si riflette in un ritardo temporale $\delta = k \Delta$ di ciascun full-adder.

Nel ripple carry adder, il riporto viene propagato nella catena $\Rightarrow$ i ritardi crescono al crescere del parallelismo.

\subsubsection{Sommatore combinatorio modulare con carry-lookahead}
Dalla funzione del sommatore combinatorio modulare relativa al riporto $c_i$:
\[
 \begin{cases} c_i = x_i y_i + (x_i + y_i) c_{i-1} = g_i + p_i c_{i-1} \\
c_{i-1} = x_i y_i + (x_i + y_i) c_{i-2} = g_i + p_i c_{i-2} \end{cases} \Rightarrow c_i = g_i + p_i g_{i-1} + p_i p_{i-1} c_{i-2}
\]
con: $\begin{cases} g_i = x_i \cdot y_i \\
p_i = x_i + y_i \end{cases}$, si può ricavare la seguente formula in termini solo di $x$ e $y$:
\[
 c_k = g_k + (p_k g_{k-1} + p_k p_{k-1} g_{k-2} + \cdots + p_k p_{k-1} \cdots p_2 p_1 g_0) + p_k p_{k-1} \cdots p_2 p_1 p_0 c_{in}
\]
in modo che il full-adder non debba aspettare il carry del precedente. Il \textbf{carry-lookahead} invia a ciascun full-adder il corrispondente $c$.

\subsubsection{Sommatore seriale}
Il \textbf{sommatore seriale} contiene un singolo full-adder che tratta a ogni colpo di clock una coppia di bit alla volta; il carry corrente viene salvato in un flip-flop interno.

\paragraph{Vantaggi} minimo hardware e minimo costo

\paragraph{Svantaggio} i tempi di esecuzione sono più lunghi a causa dei colpi di clock

\subsubsection[ALU]{ALU\footnote{Per approfondire, si veda la voce \href{https://it.wikipedia.org/wiki/Arithmetic_Logic_Unit}{Arithmetic Logic Unit} su Wikipedia in italiano.}}
\label{sez:alu}
La \textbf{Arithmetic Logic Unit} (ALU) è una componente combinatoria che implementa tutte le principali funzioni aritmetiche (su numeri decimali) e logiche (bit a bit). In ingresso non ha il segnale di clock, ma un segnale di controllo che specifica il tipo di operazione da effettuare. Il ritardo è dovuto solo alle porte logiche interne. Un ulteriore ingresso di carry $C_\text{in}$ (con corrispettiva uscita $C_\text{out}$) permette di collegare ALU in cascata per operazioni più complesse.

\subsection{Comparatore a 4 bit}
Il \textbf{comparatore a 4 bit} permette di confrontare due numeri interi senza segno su 4 bit. Un valore di enable stabilisce se le linee sono attivabili.

Connettendo più comparatori in cascata è possibile confrontare interi composti da un numero maggiore di bit: si va dalla quaterna di bit più significativa a quella meno significativa. Il segnale di enable in ingresso a ogni comparatore è pilotato dal comparatore precedente nella catena, in modo che venga attivato solo se quest'ultimo ha verificato la condizione di uguaglianza.

\subsection{Registri}
\subsubsection{Registro a \texorpdfstring{$n$}{n} bit}
\label{sez:registro_n_bit}
Il \textbf{registro a $n$ bit} serve per memorizzare un ingresso su $n$ bit ogniqualvolta viene attivato il segnale di load; il valore memorizzato viene riportato nell'uscita $z$ e viene mantenuto costante fino al successivo segnale di load $\Rightarrow$ è possibile stabilizzare un segnale molto variabile. Se presente, il segnale di clear permette di resettare a 0 tutti i bit memorizzati.

Internamente, il registro a $n$ bit contiene una serie di $n$ flip-flop di tipo $D$, ciascuno dei quali memorizza uno degli $n$ bit della parola in ingresso.

\subsubsection{Registro a scalamento}
Il \textbf{registro a scalamento} (o shift-register) memorizza le parole non parallelamente ma un bit alla volta, perché l'ingresso $x$ e l'uscita $z$ hanno parallelismo 1.

Il segnale di shift enable pilota il clock di ogni flip-flop: al segnale di shift enable, il contenuto corrente viene scalato di una posizione: il flip-flop $i$-esimo passa il bit memorizzato al flip-flop $i+1$-esimo, quindi riceve il nuovo bit dal flip-flop $i-1$-esimo, facendo entrare all'estrema sinistra il nuovo bit in ingresso e facendo uscire all'estrema destra l'ultimo bit in eccesso.

I registri a scalamento universali (più costosi) permettono lo scalamento in entrambi i sensi, e ne esistono diverse implementazioni:
\begin{itemize}
\item ci possono essere due segnali di shift enable, uno a destra e uno a sinistra;
\item ci può essere un unico segnale di shift enable insieme a uno di left shift select, che specifica se effettuare lo scalamento a destra o a sinistra.
\end{itemize}

Vi può anche essere un segnale di load che permette di caricare parallelamente $n$ bit in ingresso (come nel registro a $n$ bit).

\paragraph{Applicazioni}
\begin{itemize}
 \item convertire dati seriali in ingresso (un bit per volta) in dati paralleli in uscita (byte), e viceversa;
\item moltiplicare/dividere numeri per potenze di 2 (basta scalare di un bit);
\item memorizzare dati seriali in una coda FIFO (il primo valore scala fino in fondo ed esce per primo).
\end{itemize}

\subsection{Contatori}
In un \textbf{contatore}, il valore memorizzato all'interno viene incrementato/decrementato a ogni segnale di count enable. Un contatore può avere, a seconda dell'occasione, anche degli altri segnali specifici:
\begin{itemize}
\item up/down: indica se il valore va incrementato o decrementato;
\item step: specifica di quanto deve essere incrementato il valore;
\item terminal count: fornisce 1 quando il contatore raggiunge il suo valore massimo (11\textellipsis ).
\end{itemize}

Viene anche detto \textbf{contatore modulo-$\bm{2^k}$} (dove $k$ è il numero di bit) perché al $2^k$-esimo impulso (11\textellipsis ) ritorna al valore iniziale 0.

\subsubsection{Contatore asincrono}
Il \textbf{contatore asincrono} è costituito da una serie di flip-flop di tipo $T$: il segnale di count enable di ogni flip-flop è pilotato dal flip-flop precedente, così un flip-flop quando commuta da 0 a 1 iniziando il fronte di salita dell'uscita fa commutare anche il flip-flop successivo $\Rightarrow$ il contatore è asincrono perché i flip-flop non sono pilotati dallo stesso segnale di clock. La successione di bit ottenuta è discendente.

\paragraph{Vantaggi} hardware economico e facilità di progettazione

\paragraph{Svantaggio} siccome ogni flip-flop ha un ritardo $\Delta$, il ritardo complessivo è proporzionale al parallelismo del contatore (numero di flip-flop)

\subsubsection{Contatore sincrono}
Il \textbf{contatore sincrono} prevede un unico segnale di count enable. La logica combinatoria è costituita da un sommatore/sottrattore che carica parallelamente tutti i bit, li elabora e restituisce direttamente tutti i nuovi bit da scrivere nei flip-flop.

\paragraph{Svantaggi} hardware più complesso e costoso

\paragraph{Vantaggio} il ritardo è indipendente dal numero di flip-flop, e dipende solo dai ritardi della logica combinatoria e del singolo flip-flop

\subsection{Interfacce al bus}
\label{sez:interfacce_bus}
Un insieme di moduli è detto \textbf{in connessione a bus} se i segnali vengono trasferiti da un modulo all'altro attraverso una struttura di interconnessione/comunicazione detta \textbf{bus}\footnote{Si veda il capitolo~\ref{cap:bus}.}, da cui i moduli in lettura caricano il valore corrente a ogni segnale di load. Per i moduli in scrittura invece vale la seguente regola: in ogni istante il bus può essere pilotato da uno solo dei moduli collegati in scrittura $\Rightarrow$ ogni modulo deve essere collegato al bus attraverso un'interfaccia.

L'\textbf{interfaccia} è un dispositivo, posto tra il modulo e il bus, che prima di passare al bus il nuovo valore da scrivere verifica che nessun altro modulo stia scrivendo sul bus.

Il \textbf{buffer tri-state} è un'interfaccia che agisce a seconda dei valori di enable, pilotati da un decoder:
\begin{itemize}
\item $e= 1$: il modulo è collegato al bus, e l'interfaccia lascia passare l'uscita del modulo verso il bus;
\item $e = 0$: il modulo è in \textbf{alta impedenza} (l'uscita è $Z$), cioè è elettricamente scollegato dal bus e la scrittura è inibita.
\end{itemize}

Il \textbf{transceiver} è un'interfaccia generica che permette la bidirezionalità del segnale (sia in lettura sia in scrittura).

\subsection[Memorie]{Memorie\footnote{Per approfondire, si veda il capitolo~\ref{cap:memorie_accesso_casuale}.}}
Le \textbf{memorie} sono dei moduli/dispositivi in grado di memorizzare $m$\footnote{Solitamente il numero di parole $m$ è una potenza di 2.} parole di parallelismo $n$ e di mantenerle nel tempo. A differenza dei registri, le memorie possono leggere o scrivere da/su una sola parola per volta. Il segnale di indirizzo su $\log_2{m}$ bit fornisce l'indice della parola; i segnali di read e di write specificano se la parola è da leggere o scrivere. Manca il segnale di clock $\Rightarrow$ le specifiche devono indicare i parametri temporali della memoria, come il tempo minimo di attesa tra un'operazione e l'altra e il tempo di lettura della parola in ingresso, in base ai ritardi interni della memoria.

Un segnale di enable serve per accecare la memoria ai segnali di ingresso e per eventualmente mettere l'uscita a un bus in alta impedenza.

\subsubsection{Memoria RAM}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{pic/02/Matrice_di_memoria.png}
	\caption{Struttura interna di una memoria RAM.}
\end{figure}

\noindent
Le \textbf{memorie RAM} sono delle memorie cosiddette \textbf{volatili} perché il contenuto viene cancellato quando si scollega l'alimentazione. Le memorie RAM si possono inserire internamente in un circuito oppure su piastre.

\paragraph{Operazione di scrittura}
Un decoder riceve in ingresso il segnale di indirizzo e seleziona una parola attivando uno degli $m$ segnali di load, il quale, per mezzo di una porta AND che lo affianca al segnale di write, attiva la scrittura nella parola di indice corrispondente.
\FloatBarrier%WARNING

\paragraph{Operazione di lettura}
Il multiplexer, ricevendo il segnale di indirizzo (sempre accoppiato in AND con il segnale di read), seleziona una parola e ne riporta l'uscita.
%\FloatBarrier

\subsubsection{Memoria ROM}
A differenza delle RAM, le \textbf{memorie ROM} sono in sola lettura. Il contenuto viene determinato all'atto della fabbricazione e non può più essere modificato successivamente. La memoria ROM è un modulo combinatorio perché l'uscita dipende solo dall'ingresso dell'indice di parola corrente.

\subsubsection{Banchi di memoria}
Se la lunghezza delle parole è maggiore del parallelismo di un modulo, si possono affiancare più moduli su cui suddividere le parole, a cui fornire le opportune porzioni di bit e parallelamente il segnale di indirizzo comune.

Se invece la suddivisione è fatta sullo spazio di indirizzamento, i dati vengono forniti parallelamente a tutti i moduli tramite un bus. Come segnali di enable si devono usare direttamente i bit più significativi degli indirizzi di memoria, che possono essere elaborati da un decoder o più semplicemente, nel caso di 2 moduli, inviati in via diretta a un modulo e attraverso un inverter all'altro.

\subsection{Moduli programmabili}
\subsubsection{PLA}
Le \textbf{Programmable Logic Array} (PLA) aiutano a costruire un circuito di tipo combinatorio in modo più semplice ed economico: il costruttore può infatti programmare una PLA, cioè può collegare le singole porte logiche non direttamente a mano ma attraverso dei segnali elettrici, in modo da personalizzarla in base alla funzione booleana che vuole implementare. La programmazione di una PLA è irreversibile perché si può fare una volta sola.

Un esempio di applicazione è la codifica di un numero in una visualizzazione a trattini sul display del supermercato.

\subsubsection{FPGA}
Il \textbf{Field Programmable Gate Array} (FPGA) aggiunge alla PLA una componente sequenziale e la possibilità di riprogrammare più di una volta il circuito. Rappresenta una soluzione intermedia tra il costo di progettazione e l'efficienza della soluzione HW e la flessibilità della soluzione SW.

L'FPGA viene utilizzata spesso per i prototipi: il programmatore può sperimentare il suo codice sull'FPGA prototipo mentre aspetta che venga sviluppato il più ottimizzato circuito integrato ASIC (= soluzione HW).

\section{Cenni sul consumo}
Il consumo nel tempo di corrente da parte di un circuito è definito in termini di:
\begin{itemize}
\item energia totale (potenza effettiva)
\item dissipazione del calore
\end{itemize}

\paragraph{Parametri}
\begin{itemize}
\item \textbf{consumo statico:} (trascurabile) è fisso perché si deve solo al fatto di essere alimentato, anche se i transistor non commutano;
\item \textbf{consumo dinamico:} dovuto ai transistor che commutano un numero di volte proporzionale alla frequenza di clock $\Rightarrow$ si deve ridurre il più possibile, facendo attenzione alle prestazioni.
\end{itemize}