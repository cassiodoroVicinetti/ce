\chapter{I bus}
\label{cap:bus}
Il \textbf{bus} è la struttura di interconnessione condivisa tra la CPU e il resto del sistema (memoria, periferiche). È composto da:
\begin{itemize}
\item bus di indirizzi: segnali di indirizzo
\item bus dati: segnali di dato
\item bus controllo: segnali di controllo
\end{itemize}

Solo un dispositivo per volta può scrivere sul bus; possono essere più di uno i dispositivi che leggono dal bus. Quando un dispositivo diventa il \textbf{bus master} (o master del bus), prende il controllo del bus e decide a ogni ciclo il tipo di operazione da effettuare (ad es. ciclo di lettura dalla memoria, ciclo di scrittura su un periferico, ciclo di Interrupt Acknowledge).

\section{Implementazione}
\begin{itemize}
\item bus interno a un circuito integrato (ad es. il processore);
\item pista a bordo di una scheda su cui sono montati più circuiti integrati;
\item \textbf{bus di backplane}: può collegare più schede.
\end{itemize}

In un \textbf{bus multiplexato} le stesse linee portano a seconda dei momenti segnali di dato o di indirizzo $\Rightarrow$ si risparmiano linee di bus, ma è necessaria una circuiteria per la connessione alla memoria e la velocità è minore rispetto ai bus non multiplexati.

Nelle \textbf{architetture a bus multiplo}, più bus sono organizzati gerarchicamente in base alla velocità e sono connessi tra loro attraverso dei processori di IO oppure dei \textbf{bridge}, che si occupano al momento opportuno di leggere il segnale dal bus di livello superiore e di restituirlo al bus di livello inferiore.

Da/su un \textbf{bus condiviso} possono leggere/scrivere più moduli. I \textbf{bus dedicati} sono impropriamente strutture di comunicazione tra due moduli. Il bus condiviso è più economico, ma ha prestazioni meno elevate rispetto al bus dedicato, perché non può supportare più di una comunicazione in contemporanea.

\section{Sincronizzazione}
\subsection{Bus sincroni}
Tutti i dispositivi collegati a un \textbf{bus sincrono} condividono un unico segnale di clock, e la frequenza di clock è imposta dal dispositivo più lento. È preferibile che non ci siano dei dispositivi troppo lenti o troppo lontani tra di loro o dalla sorgente di clock a causa dei ritardi fisici del segnale.

Ogni volta che un dispositivo slave non riesce a completare un'operazione sul bus entro il tempo prestabilito, il bus master deve aggiungere dei colpi di clock, chiamati \textbf{cicli di wait}, fino a quando il dispositivo non completa l'operazione e non attiva il segnale di ready. I cicli di wait devono essere però richiesti il meno possibile, ad esempio da dispositivi acceduti raramente o da memorie impegnate nel refreshing.

\subsection{Bus asincroni}
La soluzione a \textbf{bus asincrono} non prevede un segnale di clock comune, ma ogni ciclo di bus è delimitato dai segnali di controllo strobe e acknowledge:
\begin{itemize}
\item \textbf{strobe:} il dispositivo sorgente comunica che il segnale è stato scritto sul bus;
\item \textbf{acknowledge:} il dispositivo di destinazione comunica che ha terminato la lettura del segnale dal bus.
\end{itemize}

La durata del trasferimento dipende solo dalle velocità dei due dispositivi coinvolti. La soluzione sincrona è più complessa e costosa rispetto a quella asincrona perché sono necessari dei segnali di controllo e una logica che li piloti.

\section{Arbitraggio}
\label{sez:arbitraggio_bus}
Quando una risorsa è condivisa, serve un meccanismo di arbitraggio per gestire le situazioni di contesa:
\begin{itemize}
 \item \textbf{arbitraggio del bus:} la CPU e il DMA Controller\footnote{Si veda la sezione~\ref{sez:DMA}.} vogliono diventare bus master nello stesso momento, ma ad ogni istante un solo dispositivo può funzionare da master del bus;
 \item più processori voglio fare accesso in contemporanea a un disco fisso;
 \item più dispositivi vogliono effettuare una richiesta di interruzione al processore (si veda la sezione~\ref{sez:interrupt}).
\end{itemize}

\subsection{Arbitraggio distribuito}
L'\textbf{arbitraggio distribuito} non prevede alcun arbitro. Nel caso del \textbf{bus SCSI}, ad esempio, ogni dispositivo che vuole essere promosso a bus master deve prima accertarsi che non esista un altro dispositivo a priorità più alta (cioè su una linea \texttt{DB(\textsl{i})} con numero \texttt{\textsl{i}} maggiore) che a sua volta intende diventare il bus master.

\subsection{Arbitraggio centralizzato}
L'\textbf{arbitraggio centralizzato} prevede un arbitro che decide chi promuovere a bus master per il prossimo ciclo di bus. Tutti i dispositivi sono collegati al segnale Bus Busy che indica se il bus è attualmente libero o occupato da un bus master.

\subsubsection{Richieste indipendenti}
Con il meccanismo delle \textbf{richieste indipendenti} ogni dispositivo è collegato all'arbitro tramite una coppia di segnali di controllo:
\begin{itemize}
\item \textbf{Bus Request:} il dispositivo richiede di essere promosso a bus master;
\item \textbf{Bus Grant} (o Acknowledge): l'arbitro concede al dispositivo la promozione a bus master.
\end{itemize}

È una soluzione costosa perché servono molti segnali di controllo e un arbitro che implementi una strategia intelligente.

\subsubsection{Daisy Chaining}
Nel \textbf{Daisy Chaining} i vari segnali Bus Request delle unità sono collegati in wired-or\footnote{Si veda la sezione~\ref{sez:terminologia}.}. Quando arriva una richiesta il Bus Grant, segnale unico, inizia a scorrere nella catena delle unità alla ricerca di quella che ha fatto la richiesta:
\begin{itemize}
\item se l'unità corrente è quella che ha fatto la richiesta, essa attiva il Bus Busy e prende il controllo del bus;
\item se l'unità corrente invece non ha fatto richiesta, passa il Bus Grant all'unità successiva.
\end{itemize}

È una soluzione semplice ed economica perché il numero dei segnali è indipendente dal numero di unità, ma presenta delle caratteristiche svantaggiose:
\begin{itemize}
\item \ul{poco flessibile:} le priorità sono fisse, e l'ultimo dispositivo nella catena ha la priorità minima;
\item \ul{lenta:} il Bus Grant impiega un certo tempo a scorrere la catena;
\item \ul{poco affidabile:} se un'unità precedente nella catena smette di funzionare, la catena si interrompe.
\end{itemize}

\subsubsection{Polling}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{pic/14/Arbitraggio_bus_centralizzato_con_polling.png}
	\caption{Esempio di arbitraggio centralizzato con polling.}
\end{figure}

\noindent
La soluzione con \textbf{polling} differisce dal Daisy Chaining per il meccanismo del Bus Grant. Quando il bus si libera e almeno un'unità ha fatto la richiesta (tramite connessione wired-or), l'arbitro scandisce la catena interrogando i dispositivi uno alla volta, e si ferma al primo che risponde e attiva il Bus Busy. A ogni unità è associato un codice identificativo binario, che durante la scansione della catena viene fornito dall'arbitro su un'altra linea a cui sono collegate tutte le unità.

\paragraph{Vantaggi}
\begin{itemize}
\item maggiore flessibilità all'interno dell'arbitro: cambiando l'ordine con cui l'arbitro scandisce i dispositivi è possibile implementare qualsiasi meccanismo di gestione delle priorità;
\item maggiore tolleranza ai guasti: se un'unità si guasta le altre continuano a funzionare.
\end{itemize}
%\FloatBarrier