\chapter{Introduzione alle memorie}
Accanto al processore e alle periferiche vi è un \textbf{sotto-sistema di memoria}. Si deve cercare un compromesso tra:
\begin{itemize}
\item prestazioni, ovvero il tempo di accesso a dati e istruzioni da parte del processore;
\item costo.
\end{itemize}

Siccome la velocità dei processori raddoppia ogni anno e mezzo mentre quella delle memorie (in particolare i colpi di clock di attesa a ogni accesso) ogni dieci, gli accessi in memoria penalizzano sempre di più le prestazioni di processori veloci.

\section{Livelli di memorie}
\label{sez:livelli_memorie}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{pic/06/Piramide_memorie.png}
	\caption{Piramide dei supporti di memorizzazione.}
\end{figure}

\noindent
Le memorie sono caratterizzate dalla \textbf{località dei riferimenti} (= accessi): le applicazioni non accedono uniformemente all'intera memoria, ma statisticamente nella finestra temporale l'esecuzione delle istruzioni rimane limitata entro una certa zona della memoria (su istruzioni vicine), e analogamente la lettura/scrittura di dati interessa per un certo tempo solo alcuni dei dati. Perciò la maggior parte degli accessi in memoria non avviene in celle isolate, ma a un blocco di celle di memoria consecutive:
\begin{itemize}
\item \textbf{località temporale:} se all'istante $t$ il programma accede a una cella di memoria, è probabile che nell'istante successivo $t+D$ acceda di nuovo alla stessa cella;
\item \textbf{località spaziale:} se all'istante $t$ il programma accede alla cella di memoria all'indirizzo $X$, è probabile che nell'istante successivo $t+D$ acceda alle celle vicine all'indirizzo $X+e$.
\end{itemize}

L'accesso allo stack è esclusivamente locale: le celle vengono possono essere accedute solo in modo consecutivo tramite le operazioni di push e pop.

Si possono quindi ottimizzare le prestazioni della memoria suddividendola in più \textbf{livelli}: le memorie di livello superiore, più piccole ma più veloci e costose per bit, sono destinate al blocco di dati e istruzioni su cui l'applicazione sta operando correntemente. Si deve però usare un meccanismo efficiente per posizionare i dati e le istruzioni nel livello corretto.

Al primo livello si trovano i registri, che costituiscono la \textbf{memoria interna} del processore: sono realizzati con RAM statiche e sono estremamente costosi.

All'interno della modulo RAM si distinguono una \textbf{memoria cache} veloce e la \textbf{memoria principale} vera e propria, realizzata con RAM dinamiche. Altre memorie cache sono utili all'interno del processore o montate sulla scheda madre.

La \textbf{memoria secondaria} è molto più capiente ma molto più lenta (es. dischi fissi). L'accesso è molto più complesso: il programma non può accedervi direttamente, ma richiede la chiamata a procedure offerte dal sistema operativo.

\textbf{Memorie off-line} come le chiavette richiedono anche operazioni meccaniche per l'accesso (es. inserimento nella porta USB).
\FloatBarrier

\section{Parametri}
\subsection{Costo}
Più aumenta la velocità più aumenta il costo. Quando bisogna minimizzare le dimensioni fisiche (es. SoC), si somma il costo di progetto.

\subsection{Prestazioni}
\subsubsection{Tempo di accesso}
Il \textbf{tempo di accesso} (o latenza) è il tempo richiesto per soddisfare la richiesta dell'utente di lettura o scrittura:
\begin{itemize}
\item lettura: è il tempo che intercorre tra l'istante in cui il processore fornisce l'indirizzo e l'istante in cui la memoria fornisce l'MFC, che è il segnale di controllo che avvisa che il dato è stato letto;
\item scrittura: è il tempo che intercorre tra l'istante in cui il processore fornisce il dato e l'istante in cui la memoria riceve il dato.
\end{itemize}

Un'efficiente gestione dei livelli di memoria riduce i tempi di accesso: i dischi magnetici e ottici hanno infatti un elevato tempo di accesso a causa di limitazioni meccaniche.

Alla fine dell'accesso non è detto che la memoria sia già pronta per iniziare un altro ciclo, cioè per leggere/scrivere un nuovo dato.

\subsubsection{Tempo di ciclo}
Il \textbf{tempo di ciclo} è il tempo che intercorre tra un ciclo di accesso e l'altro, cioè tra l'istante in cui la memoria principale riceve la richiesta di lettura/scrittura e l'istante in cui la memoria risulta pronta per una nuova lettura/scrittura. Ad esempio, nella scrittura è necessario aspettare che la memoria, dopo aver ricevuto il dato dal processore, lo scriva internamente. Il tempo di ciclo è quindi più lungo del tempo di accesso, o al più uguale.

\subsubsection{Tasso di trasferimento}
L'accesso alle memorie secondarie non avviene a singoli byte ma a blocchi (dell'ordine di kilobyte), perché si devono effettuare delle operazioni meccaniche preliminari di preparazione che richiedono un primo tempo di accesso per avviare la lettura/scrittura del blocco. Pertanto si deve cercare di minimizzare il numero degli accessi sfruttando il più possibile l'accesso a blocchi $\Rightarrow$ il \textbf{tasso di trasferimento} di una memoria secondaria è la velocità di lettura sequenziale del blocco, e si misura in bit per secondo (bps).

\subsection{Altri parametri}
\begin{itemize}
\item consumo
\item portabilità
\item dimensione
\end{itemize}

\section{Modi di accesso}
\begin{itemize}
\item \textbf{accesso casuale:} (es. RAM) ogni byte ha un indirizzo, e il tempo di accesso è lo stesso per qualsiasi byte;\footnote{Si veda il capitolo~\ref{cap:memorie_accesso_casuale}.}
\item \textbf{accesso sequenziale:} (es. nastri) il tempo di accesso dipende da quanto l'ordine di accesso segue l'ordine in cui sono memorizzati i dati;
\item \textbf{accesso diretto:} (es. disco) ogni blocco ha un indirizzo, e l'accesso è casuale per l'individuazione del blocco e sequenziale all'interno del blocco stesso;
\item \textbf{accesso associativo:} a ogni parola è associata una chiave binaria; le memorie ad accesso associativo sono molto costose.
\end{itemize}

\section{Conservazione dei dati}
\label{sez:conservazione_dati}
L'\textbf{alterabilità} è la possibilità di modificare la memoria: le ROM non hanno alterabilità, le RAM sì.

La \textbf{volatilità} è l'incapacità di mantenere i dati memorizzati alla rimozione dell'alimentazione.

In una memoria con tecnologia \textbf{Destructive Readout}, i dati dopo che vengono letti vengono cancellati dalla memoria $\Rightarrow$ vanno persi se non vengono riscritti successivamente in memoria.

Alcune memorie dinamiche hanno bisogno periodicamente di operazioni di \textbf{refreshing}, cioè di lettura e immediata riscrittura di tutti i dati, affinché non vadano persi.

Le memorie possono avere dei guasti, cioè delle differenze di comportamento da quello previsto; comprendono mancanze di risposta o bit scritti con valore sbagliato (per cause come le radiazioni). I \textbf{guasti permanenti} persistono a ogni tentativo, e non sono più riparabili; i \textbf{guasti transitori} capitano una volta sola. Le frequenze di guasto si dicono rispettivamente \textbf{Mean Time To Failures} (MTTF) e \textbf{Mean Time Between Failures} (MTBF).