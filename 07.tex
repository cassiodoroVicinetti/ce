\chapter{Le memorie ad accesso casuale}
\label{cap:memorie_accesso_casuale}
Per le \textbf{memorie ad accesso casuale} vale:
\begin{itemize}
 \item ogni cella può essere indirizzata indipendentemente;
 \item i tempi di accesso sono uguali e costanti per ogni cella.
\end{itemize}

\section{Segnali di controllo e linee di stato}
Il funzionamento della memoria è temporizzato: occorrono un certo tempo minimo per stabilizzare i segnali di dato e di indirizzo, un certo tempo minimo per accedere a quei segnali, un certo tempo minimo tra un accesso e l'altro, ecc. Il progettista può scegliere semplicemente di affidarsi ai tempi stabiliti dalle specifiche, oppure aggiungere dei circuiti appositi per valutare dinamicamente i segnali di controllo. I \textbf{segnali di controllo} servono per esempio:
\begin{itemize}
\item per informare la memoria sul tipo di operazione corrente (lettura o scrittura);
\item per informare la memoria quando sono stabili e possono essere campionati i segnali di indirizzo sull'\textbf{Abus} e di dato sul \textbf{Dbus} (in scrittura);
\item per informare il processore quando sono stabili e possono essere campionati i segnali di dato sul Dbus (in lettura);
\item per informare il processore quando è possibile procedere con un nuovo ciclo di accesso alla memoria;
\item per informare il processore se la memoria sta effettuando le operazioni di refreshing e i dati in lettura non sono stabili.
\end{itemize}

Accanto ai segnali di controllo vi sono le \textbf{linee di stato}, che sono dei segnali per informare l'esterno sullo stato corrente della memoria. Si usano generalmente per informare che si è verificato un guasto. Non comprendono l'MFC, perché esso non viene generato dalla memoria ma dal controllore della memoria.

\section{Struttura interna}
I bit effettivi che memorizzano le informazioni sono contenuti nella \textbf{matrice di memoria}. Il \textbf{circuito di controllo} riceve tramite il bus di controllo i segnali di controllo del processore (ad esempio i segnali di read e write), quindi pilota vari moduli all'interno della memoria, garantendo che i segnali caricati nei registri siano sempre stabili. Due registri di dato permettono la comunicazione con il bus di dati; ci può anche essere un transceiver\footnote{Si veda la sezione~\ref{sez:interfacce_bus}.} che permette la bidirezionalità del segnale.

\subsection{Organizzazione a vettore}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{pic/07/Struttura_interna_RAM.png}
	\caption{Struttura interna di una memoria ad accesso casuale con organizzazione a vettore.}
\end{figure}

\noindent
La memoria è vista come un vettore di parole: l'indirizzo proveniente dal bus di indirizzi, di parallelismo $\log_2{n}$, viene stabilizzato da un registro\footnote{Si veda la sezione~\ref{sez:registro_n_bit}.} degli indirizzi, pilotato tramite segnali di controllo dal circuito di controllo, e viene decodificato da un \textbf{decoder degli indirizzi} in $n$ segnali di enable che selezionano la parola corretta all'interno della matrice di memoria. È semplice da implementare se il numero delle parole è ragionevole, altrimenti il decoder diventerebbe troppo complesso.
\FloatBarrier

\subsection{Organizzazione a matrice bidimensionale}
La memoria è vista come una matrice organizzata in righe e colonne i cui elementi sono delle parole, ognuna delle quali ha un segnale di selezione di riga e uno di colonna, provenienti da due decoder. La parola viene attivata solamente se entrambi i segnali di selezione sono attivi. Due decoder di selezione risultano molto più semplici di un singolo decoder. Si può inoltre dimezzare il numero di segnali di indirizzo grazie a due registri: prima si salva l'indirizzo di riga in un registro, poi si salva l'indirizzo di colonna nell'altro registro, e infine si attivano i decoder.

I \textbf{segnali di strobe} \texttt{RAS} e \texttt{CAS} sono segnali di controllo che specificano se gli indirizzi devono essere caricati sul registro delle righe o delle colonne.

Si possono perciò velocizzare gli accessi sequenziali a blocchi di memoria (bit consecutivi), che sono statisticamente molto frequenti: la possibilità di attivare separatamente i registri di caricamento degli indirizzi permette di mantenere lo stesso indirizzo di riga al variare degli indirizzi di colonna (\textbf{Page Mode}).

\section{Memorie non volatili}
\subsection{Memorie ROM}
\begin{figure}
	\centering
	\includegraphics[width=0.3\linewidth]{pic/07/Cella_ROM.png}
	\caption{Cella di una memoria ROM.}
\end{figure}

\noindent
Il contenuto delle \textbf{memorie ROM} è definito solamente al momento della costruzione e successivamente può essere acceduto solo in lettura. Applicazioni:
\begin{itemize}
 \item librerie di procedure frequentemente usate;
\item programmi di sistema;
\item \textbf{tavole di funzioni:} sono gli elenchi dei risultati di funzioni che sarebbero troppo complessi da calcolare sul momento.
\end{itemize}

In fase di costruzione della memoria si agisce sul punto P che si trova tra ogni transistor e la massa: la cella assume il valore 1 se il punto P diventa un circuito aperto, e assume il valore 0 se P è un cortocircuito.

La variazione del costo è trascurabile al variare del numero di chip all'interno del singolo dispositivo rispetto al numero di dispositivi prodotti; si somma il costo di progetto. La memoria ROM è \textbf{non volatile}: il contenuto non viene cancellato all'assenza dell'alimentazione.
\FloatBarrier

\subsection{Memorie PROM}
Nelle \textbf{memorie PROM}, agli incroci delle linee vi sono dei diodi che possono essere bruciati selettivamente tramite delle correnti e trasformati in circuiti aperti. Possono essere programmate una sola volta, prima di metterle sulla piastra.

\subsection{Memorie EPROM}
Le \textbf{memorie EPROM} possono essere riprogrammate più volte, anche se la cancellazione avviene grazie a raggi ultra-violetti. La programmazione può essere effettuata anche sulla piastra, poiché le correnti necessarie sono compatibili con la piastra stessa.

\subsection{Memorie EEPROM}
Le \textbf{memorie EEPROM} possono essere riprogrammate senza raggi ultra-violetti, ma la programmazione è lenta e sono costose.

\subsection{Memorie flash}
Le \textbf{memorie flash} richiedono delle operazioni di scrittura a blocchi, e il blocco deve essere ogni volta cancellato prima di poter essere riscritto.

\section{Memorie RAM}
Le memorie RAM possono essere di due tipi:
\begin{itemize}
\item \textbf{memorie statiche:} ogni bit richiede un flip-flop \textit{SR} (6 transistor CMOS);
\item \textbf{memorie dinamiche:} ogni bit richiede un condensatore e un transistor $\Rightarrow$ più densa.
\end{itemize}

\subsection{Memorie RAM statiche}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/07/Cella_RAM_statica.png}
	\caption{Cella di una memoria RAM statica.}
\end{figure}

\noindent
Ogni cella di una \textbf{memoria RAM statica} richiede 6 transistor: 4 costituiscono una combinazione di porte logiche, equivalente a un flip-flop \textit{SR}\footnote{Si veda la sezione~\ref{sez:flip_flop_asincroni}.} avente due uscite \textit{Y}\textsubscript{1} e \textit{Y}\textsubscript{2} opposte tra loro, e 2 collegano il flip-flop a due linee di dato parallele:
\begin{itemize}
\item operazione di lettura: il flip-flop forza sulle linee di dato una coppia di bit opposti che viene poi elaborata dalla circuiteria associata alla colonna;
\item operazione di scrittura: la circuiteria converte il bit nella corretta coppia di bit opposti che viene forzata sul flip-flop.
\end{itemize}
\FloatBarrier

\subsection{Memorie RAM dinamiche}
\label{sez:memorie_ram_dinamiche}
\begin{figure}
	\centering
	\includegraphics[width=0.3\linewidth]{pic/07/Cella_RAM_dinamica.png}
	\caption{Cella di una memoria RAM dinamica.}
\end{figure}

\noindent
Le celle di una \textbf{memoria RAM dinamica} contengono dei condensatori collegati alle linee di dato tramite transistor:
\begin{itemize}
\item operazione di scrittura: il condensatore viene caricato o scaricato alla tensione della linea di dato;
\item operazione di lettura: un sensore rileva se la linea di dato rimane a un valore basso perché il condensatore è scarico (valore 0) o se sale a un valore alto perché è carico (1) $\Rightarrow$ la memoria RAM dinamica è caratterizzata dal Destructive Readout\footnote{Si veda la sezione~\ref{sez:conservazione_dati}.}: se il condensatore è carico, si scarica e perde il bit 1 memorizzato $\Rightarrow$ al termine di ogni ciclo di lettura è necessario riscrivere il dato appena letto.
\end{itemize}

\paragraph{Svantaggio} le memorie dinamiche sono più lente delle memorie statiche a causa dei processi di carica e scarica del condensatore
\paragraph{Vantaggio} le memorie dinamiche sono meno costose perché ogni cella richiede un solo transistor
\FloatBarrier

I condensatori hanno una capacità infinitesima $\Rightarrow$ qualunque minuscola particella carica (es. radiazione) può modificare il valore del condensatore (bit flip) $\Rightarrow$ meno affidabilità.

Le memorie dinamiche richiedono quindi il refreshing: il contenuto deve essere periodicamente letto e riscritto, altrimenti i condensatori carichi lentamente si scaricherebbero per via delle inevitabili piccole correnti di dispersione che comunque scorrono. Gli accessi che capitano durante le operazioni di rinfresco richiedono quindi più tempo.

Il \textbf{codice di parità} è un codice di protezione che aumenta l'affidabilità della memoria: ogni parola ha un bit in più, che indica se il numero di bit pari a 1 dev'essere pari o dispari, permettendo di verificare se c'è stato un errore e quindi un bit è stato modificato.

I \textbf{codici di Hamming} sono codici SECDED che permettono sia di rilevare sia di correggere gli errori singoli, ma si limitano a rilevare gli errori doppi (molto più rari). Questi codici richiedono $log_2{m}$ bit, con $m$ parallelismo.

\subsubsection{Memorie SDRAM}
Le \textbf{memorie Synchronous DRAM} (SDRAM) sono delle RAM sincrone temporizzate da un certo segnale di clock. Il processore quindi sa esattamente quanto tempo è richiesto per l'accesso ai dati.

\subsubsection{Memorie interlacciate}
Le memorie dinamiche spesso sono più lente rispetto alla velocità del processore, perché il tempo di accesso è più lungo dell'inverso del clock del processore. Con le \textbf{memorie interlacciate}, il processore può accedere contemporaneamente, tramite indirizzi di blocco, a un blocco di $n \cdot m$ parole distribuite su ognuna delle $n$ memorie affiancate con parallelismo $m$ bit.