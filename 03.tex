\chapter{I processori}
Il \textbf{processore} è un dispositivo capace di:
\begin{itemize}
\item elaborare istruzioni, cioè leggere delle istruzioni dalla memoria (fase di fetch) ed eseguirle (fase di esecuzione);\footnote{Si veda la sezione~\ref{sez:elaborazione_istruzioni}.}
\item interagire con il mondo esterno attraverso le porte di periferica, reagendo il più presto possibile alle segnalazioni (= richieste di interrupt) provenienti dall'esterno.\footnote{Si veda il capitolo~\ref{cap:dispositivi_io}.}
\end{itemize}

L'importanza di ciascuna di queste capacità dipende dalla destinazione d'uso del processore.

\section[Elaborazione delle istruzioni]{Elaborazione delle istruzioni\footnote{Per approfondire, si veda la voce \href{https://it.wikipedia.org/wiki/Ciclo_di_fetch-execute}{Ciclo di fetch-execute} su Wikipedia in italiano.}}
\label{sez:elaborazione_istruzioni}

\subsection{Fase di fetch}
L'\textbf{instruction set} è l'insieme delle operazioni che il processore è in grado di eseguire. A ogni operazione corrisponde una certa istruzione in memoria (es. \texttt{ADD}). Durante la \textbf{fase di fetch} il processore legge una di queste istruzioni dalla memoria tramite un bus esterno:
\begin{enumerate}
\item il \textbf{Program Counter} (PC) contiene l'indirizzo di memoria della nuova istruzione da leggere;
\item l'\textbf{Address Register} (AR) passa il nuovo indirizzo dal PC alla memoria;
\item la memoria restituisce la porzione di codice corrispondente all'indirizzo specificato dal processore;
\item il \textbf{Data Register} (DR) memorizza\footnote{Il DR viene usato in generale per il trasferimento di dati tra la memoria e il processore: un ulteriore segnale di controllo indica se l'operazione che deve effettuare la memoria è la lettura o la scrittura.} nell'\textbf{Instruction Register} (IR) la porzione di codice proveniente dalla memoria, pronta ad essere decodificata ed eseguita dal processore;
\item il PC viene aggiornato all'indirizzo dell'istruzione successiva a quella letta.
\end{enumerate}

\subsection{Fase di esecuzione}
L'\textbf{unità di controllo}\footnote{Si veda il capitolo~\ref{cap:unita_controllo}.}, tramite il bus interno, dopo aver ricevuto l'istruzione dall'IR, ha il compito di pilotare i vari moduli, cioè fornire segnali di controllo ad essi, per le operazioni ad es. di caricamento degli operandi, esecuzione delle istruzioni, acquisizione dei risultati, ecc. La \textbf{Arithmetic-Logic Unit} (ALU)\footnote{Si veda la sezione~\ref{sez:alu}.} è il modulo che esegue una serie di operazioni aritmetiche/logiche su operandi. I registri \textbf{Accumulator} (AC) forniscono gli operandi alla ALU e ricevono i risultati delle operazioni.

All'interno di ogni istruzione si possono identificare delle \textbf{microistruzioni} elementari (es. accesso ai registri, fetch, ecc.), ciascuna delle quali richiede un colpo di clock.

Il tempo di esecuzione di un'istruzione, misurato in colpi di clock, dipende da:
\begin{itemize}
\item \ul{tipo di istruzione:} quale istruzione eseguire (es. \texttt{MOV}, \texttt{ADD})?
\item \ul{tipo di operandi:} l'operando si trova nei registri (accesso più veloce) o in memoria (ritardo del fetch)?
\item \ul{modo di indirizzamento:} l'indirizzo passato è diretto (es. \texttt{VAR}) o richiede una somma (es. \texttt{vett[DI]})?
\end{itemize}

Gli \textbf{operandi} di una generica istruzione possono essere contenuti nei registri (\texttt{Rx}) o nella memoria (\texttt{M[xx]}), a seconda di ciò che è richiesto dall'operazione letta dalla memoria. I \textbf{registri} sono dei moduli di memoria costituiti da flip-flop che risiedono all'interno del processore. Anche se il numero di registri è finito, il tempo di accesso a un registro è molto minore del tempo di accesso a una cella di memoria, a causa dei colpi di clock necessari durante l'operazione di fetch. Il compilatore di C decide automaticamente se utilizzare i registri o la memoria, privilegiando quando possibile i primi: nel caso in cui l'operando si trova in memoria, durante la fase di esecuzione dell'operazione è necessario effettuare un ulteriore fetch per l'operando.

Per ridurre i tempi di esecuzione, mentre l'\textbf{execution unit} esegue un'operazione la \textbf{bus interface unit} si occupa del fetch dell'istruzione successiva.