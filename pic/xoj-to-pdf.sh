#!/bin/bash

#$1 = xournal 0.4.8 + http://lucaghio.webege.com/redirs/k

my_convert() {
  "$1" "$2.xoj" --export-pdf="$2.pdf"
  pdfcrop "$2.pdf" "$2.pdf"
}

SAVEIFS=$IFS # http://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html
IFS=$(echo -en "\n\b")

for dir in ./*/
do
  cd "$dir"
  for xoj in $(find -maxdepth 1 -type f -name "*.xoj")
  do
    my_convert "$1" "$(basename "$xoj" ".xoj")"
  done
  cd ../
done

IFS=$SAVEIFS