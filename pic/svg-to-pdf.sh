#!/bin/bash

my_convert() {
  inkscape -z -D --file="$1.svg" --export-pdf="$1.pdf"
}

SAVEIFS=$IFS # http://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html
IFS=$(echo -en "\n\b")

for dir in ./*/
do
  cd "$dir"
  for svg in $(find -maxdepth 1 -type f -name "*.svg")
  do
    my_convert "$(basename "$svg" ".svg")"
  done
  cd ../
done

IFS=$SAVEIFS