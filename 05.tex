\chapter[Unità di controllo]{Unità di controllo\footnote{Si veda la voce \href{https://it.wikipedia.org/wiki/Unità_di_controllo_(informatica)}{Unità di controllo (informatica)} su Wikipedia in italiano.}}
\label{cap:unita_controllo}
Il processore è composto da due parti:
\begin{itemize}
\item \textbf{unità di elaborazione} (o data path): contiene tutti i componenti attraverso cui passano i dati;
\item \textbf{unità di controllo} (UC): si occupa di fornire i segnali di controllo per pilotare i componenti contenuti nell'unità di elaborazione, a seconda di volta in volta della istruzione corrente contenuta nell'IR e degli stimoli provenienti dall'esterno (es. campionamento MFC\footnote{Si veda la sezione~\ref{sez:lettura_scrittura_memoria}.}).
\end{itemize}

\section{Microistruzioni}
\begin{figure}
	\centering
	\includegraphics[width=0.55\linewidth]{pic/05/Struttura_interna_CPU.png}
	\caption{Esempio di CPU.}
\end{figure}

\noindent
L'unità di controllo deve conoscere le operazioni che deve effettuare per tutte le microistruzioni dell'instruction set. L'unità di controllo può eseguire le seguenti operazioni elementari:
\begin{enumerate}
\item trasferimento di un dato da un registro ad un altro;
\item prelievo di un dato o di una istruzione dalla memoria e caricamento in un registro;
\item scrittura in memoria di un dato contenuto in un registro;
\item passaggio alla ALU degli operandi di un'operazione aritmetica o logica e memorizzazione del risultato in un registro.
\end{enumerate}
\FloatBarrier

\subsection{Trasferimento di dati tra registri}
I dati possono passare attraverso il \textbf{bus interno} da un registro all'altro. La unità di controllo può pilotare l'ingresso e l'uscita di ogni registro attraverso due segnali di controllo: uno pilota l'ingresso per il caricamento di dati dal bus, l'altro viceversa.

La unità di controllo deve anche preoccuparsi che l'ingresso del registro di destinazione venga attivato dopo che il registro di partenza ha finito di scrivere il dato sul bus.

\subsection{Lettura e scrittura di dati in memoria}
\label{sez:lettura_scrittura_memoria}
Dal punto di vista della comunicazione tra memoria e processore, il bus esterno è in realtà diviso in tre bus: bus dati, bus indirizzi e bus di controllo.

Il registro MAR può solo leggere dal bus interno e scrivere sul bus esterno tramite due segnali di controllo, e ha il compito di passare alla memoria l'indirizzo della cella da leggere. L'unità di controllo campiona il segnale di controllo MFC: quando la memoria ha finito di leggere il dato e il segnale è stabile, essa attiva l'MFC.

Il registro MDR è connesso con il bus interno e con il bus esterno con segnali bidirezionali. Un multiplexer pilotato dal segnale di controllo SEL decide se gli ingressi dell'MDR devono essere pilotati dal bus interno oppure da quello esterno. Due buffer tri-state decidono invece a quale bus inviare le uscite.

\subsection{Passaggio di operandi e ritorno del risultato di un'operazione}
Y e Z sono due registri \textbf{Accumulator} (AC) che forniscono gli operandi alla ALU e ricevono i risultati delle operazioni.

Nel caso di un'operazione a due operandi, siccome non si può leggere dalla memoria più un operando per volta, il primo operando va caricato temporaneamente nel registro Y, aspettando che il secondo operando venga letto in modo che i due operandi vengano passati parallelamente alla ALU.

Il risultato dell'operazione viene memorizzato nel registro Z pronto ad essere passato al bus interno.

\subsection{Esempi}
\subsubsection{\texttt{ADD [R3], R1}}
Durante la fase di fetch, il processore esegue in parallelo l'accesso in memoria e, mentre aspetta l'MFC, l'aggiornamento del PC (tramite una rapida PC\textsubscript{in}), sfruttando il fatto che il PC è stato caricato sul bus interno dalla precedente PC\textsubscript{out}. Inoltre, nell'attesa della lettura del secondo operando il primo viene già predisposto all'elaborazione venendo caricato nel modulo Y.

\subsubsection{\texttt{JMP lab}}
Il codice macchina dell'istruzione contiene l'offset dell'istruzione a cui saltare relativo all'indirizzo dell'istruzione di salto, pronto da sommare al valore corrente del PC.

\subsubsection{\texttt{JE lab}}
L'unità di controllo verifica il valore del generico flag N trasferito dall'unità di elaborazione all'unità di controllo (C''\textsubscript{in}).

\section{Tipi di unità di controllo}
Gli obiettivi di progettazione di un'unità di controllo sono:
\begin{itemize}
\item minimizzare la quantità di hardware;
\item massimizzare la velocità di esecuzione;
\item ridurre il tempo di progetto (flessibilità).
\end{itemize}

A seconda delle esigenze si possono scegliere due tipi di unità di controllo: \textbf{cablata} e \textbf{microprogrammata}.

Generalmente i sistemi CISC sono dotati di un'unità di controllo microprogrammata, e i RISC di una cablata. L'unità di controllo dei microprocessori 8086 è microprogrammata.

\subsection{Unità di controllo cablata}
\begin{figure}
	\centering
	\includegraphics[width=0.28\linewidth]{pic/05/Unita_di_controllo_cablata.png}
	\caption{Struttura dell'unità di controllo cablata.}
\end{figure}

\noindent
L'\textbf{unità di controllo cablata} (o hardwired) è un circuito sequenziale che riceve i segnali di ingresso, passa attraverso una variazione degli stati e restituisce i dati in uscita: dei flip-flop memorizzano lo stato corrente, e la rete combinatoria ha in ingresso l'IR e altri segnali e in uscita i segnali di controllo con cui pilotare l'unità di elaborazione.
\FloatBarrier

\begin{figure}
	\centering
	\includegraphics[width=0.28\linewidth]{pic/05/Fasi_di_fetch_in_UC_cablata.png}
	\caption{Diagramma degli stati della fase di fetch.}
\end{figure}

Le operazioni sono temporizzate da colpi di clock:
\begin{enumerate}
\item dopo aver ricevuto il segnale di \texttt{END} dell'istruzione precedente, inizia il fetch tramite il registro MAR;
\item continua a campionare il segnale MFC in attesa che la memoria sia pronta, finché l'MFC non è pari a 1;
\item carica l'istruzione nell'IR tramite il registro MDR;
\item esegue l'istruzione contenuta nell'IR pilotando i vari segnali di controllo.
\end{enumerate}
\FloatBarrier

\subsubsection{Svantaggi}
\begin{itemize}
\item \ul{complessità di progetto:} i tempi e i costi di progetto dell'unità di controllo possono diventare troppo alti se un processore è troppo complesso e richiede troppi segnali di controllo da gestire;
\item \ul{mancanza di flessibilità:} dimenticando anche solo un segnale di controllo l'operazione non va a buon fine. Inoltre, se si aggiungono istruzioni bisogna rifare da capo il progetto.
\end{itemize}

\subsection{Unità di controllo microprogrammata}
\begin{figure}
	\centering
	\includegraphics[width=0.47\linewidth]{pic/05/Unita_di_controllo_microprogrammata.png}
	\caption{Struttura (semplificata) dell'unità di controllo microprogrammata.}
\end{figure}

\noindent
L'\textbf{unità di controllo microprogrammata} ha una maggiore flessibilità di progetto, al prezzo di un maggiore costo hardware e di una minore velocità.
\FloatBarrier

\subsubsection{Memoria di microcodice}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.55\linewidth]{pic/05/Memoria_di_microcodice.png}
	\caption{Esempio di memoria di microcodice.}
\end{figure}

Le sequenze di segnali di controllo relative a ogni microistruzione non sono generate da una logica di controllo sequenziale, ma memorizzate direttamente all'interno di una \textbf{memoria di microcodice}, di solito una ROM, pilotata da un \textbf{microPC}. Un registro \textbf{microIR} memorizza temporaneamente la sequenza di bit da inviare all'unità di elaborazione. All'inizio della memoria di microcodice si trova la sequenza per la fase di fetch; successivamente sono elencati i codici di controllo delle istruzioni a cui saltare in base al campo codice operativo specificato; altre istruzioni di salto alla fase di fetch successiva sono presenti al termine di ogni istruzione. Se il progettista si accorge di avere sbagliato un valore, può correggere quel valore direttamente nella memoria senza dover riprogettare tutto da capo.

\subsubsection{Circuito di selezione dell'indirizzo}
Il \textbf{circuito di selezione dell'indirizzo} si assicura che il microPC punti sempre alla parola corretta:
\begin{itemize}
\item normalmente il circuito di selezione invia il valore del microPC a un semplice sommatore che lo incrementa all'istruzione consecutiva;
\item durante l'attesa dell'MFC, il circuito di selezione deve mantenere fisso il microPC fino a quando la memoria non è pronta;
\item il circuito di selezione valuta i flag per le istruzioni di salto condizionato:
\begin{itemize}
\item al termine della fase di fetch il microPC deve saltare alla prima microistruzione dell'istruzione letta;
\item all'ultima microistruzione dell'istruzione corrente il microPC deve tornare all'inizio della successiva fase di fetch.
\end{itemize}
\end{itemize}

All'inizio di una parola della memoria di microcodice, possono essere previsti alcuni bit riservati al circuito di selezione dell'indirizzo per aiutarlo, in termini di complessità della logica combinatoria, a decidere se la microistruzione successiva richiede un semplice incremento o un'ulteriore elaborazione del valore nel microPC. Altre soluzioni possono scegliere di specificare al termine della parola direttamente il nuovo valore da caricare nel microPC.

\subsubsection{Microprogrammazione verticale}
Il numero $m$ di parole nella memoria di microcodice è difficile da minimizzare perché dipende dal numero di microistruzioni proprie del microprocessore e dalla loro complessità in termini di colpi di clock. Il parallelismo $n$ è pari al numero dei segnali di controllo da pilotare a ogni colpo di clock, e può essere minimizzato attraverso tecniche di \textbf{microprogrammazione verticale}.

Per esempio, si possono individuare le classi di compatibilità aggiungendo un decoder in uscita di ciascuna. La \textbf{classe di compatibilità} è un gruppo di segnali di controllo a due a due \textbf{compatibili}, ovvero tra tutti i segnali ne viene attivato sempre e solo uno per colpo di clock. Un esempio è la classe di compatibilità dei segnali di controllo che pilotano i buffer tri-state connessi a un bus.

\section{Unità di controllo dell'8088}
L'unità di controllo dell'8088 è composta da una parte cablata e da una parte microprogrammata. La memoria di microcodice ha 504 parole di parallelismo 21.

In realtà, il codice operativo di ogni istruzione oltre a indicare il tipo di istruzione dà informazioni sul tipo di operandi (registro, memoria\textellipsis ). Gli operandi stessi (es. AX) vengono memorizzati all'interno dei registri M e N. Alla fine della fase di fetch, una PLA riceve il codice operativo dell'istruzione letta e restituisce il nuovo valore del microPC, selezionando l'indirizzo di partenza della sequenza di microistruzioni. Inoltre, le istruzioni che si differenziano per poche microistruzioni possono essere fatte corrispondere allo stesso blocco di memoria di microcodice, valutando quando necessario il contenuto del registro X per differenziarle.