\chapter{Le memorie ad accesso seriale}
Le \textbf{memorie ad accesso seriale} sono caratterizzate da un accesso più lento rispetto a quello delle memorie ad accesso casuale\footnote{Si veda il capitolo~\ref{cap:memorie_accesso_casuale}.}: il tempo di accesso a un blocco richiede un tempo per iniziare ad accedere al blocco e un tempo per leggere/scrivere il blocco stesso. Il tempo di accesso al blocco dipende inoltre dalla posizione del blocco.

\section{Dischi fissi}
Un \textbf{disco fisso} è un disco magnetico\footnote{Per dischi magnetici s'intendono i dischi fissi e i dischi floppy.} costituito da una pila di dischi. Su ogni faccia opera radialmente una testina; le testine sono collegate in modo solidale a un unico meccanismo di moto.

I bit sono memorizzati in maniera sequenziale su \textbf{tracce} disposte a circonferenze concentriche; ogni traccia è a sua volta suddivisa in \textbf{settori}. Per effettuare un'operazione di lettura o scrittura, la testina si posiziona sulla traccia, quindi il disco ruota per far scorrere la traccia sotto la testina.

I dischi magnetici hanno un'organizzazione \textbf{Constant Angular Velocity} (CAV): il disco gira a velocità angolare costante, indipendentemente dalla posizione della testina $\Rightarrow$ la velocità lineare dei bit è bassa vicino al centro e alta vicino al bordo $\Rightarrow$ le informazioni vanno memorizzate in modo meno denso vicino ai bordi. \ul{Vantaggio:} la circuiteria che controlla la rotazione del disco è semplice.

Il tempo di accesso $t_A$ a un generico settore è la somma di:
\begin{itemize}
\item \textbf{tempo di seek} $t_S$: la testina si posiziona sulla traccia $\Rightarrow$ dipende se la traccia si trova lontana o vicina alla testina;
\item \textbf{tempo di latenza} $t_L$: il disco ruota affinché l'inizio del settore da leggere si trovi sotto la testina $\Rightarrow$ dipende dalla velocità di rotazione del disco;
\item \textbf{'tempo di trasferimento dati} $t_D$: i dati vengono letti in modo seriale, rilevando un campo magnetico generato dalla magnetizzazione della traccia $\Rightarrow$ dipende dalla distanza tra la testina e il disco (qualche micron) e dal numero di bit da leggere/scrivere.
\end{itemize}

Non si può aumentare troppo la densità di bit per unità di superficie, ma è necessario garantire una distanza minima tra una traccia e l'altra e tra la testina e la traccia per evitare le interferenze. Inoltre, se ci si basasse semplicemente sul segno della polarizzazione (il valore alto corrisponde a 1, il valore basso corrisponde a 0), non sarebbe quantificabile il preciso numero di bit in una sequenza di bit aventi uguale polarizzazione $\Rightarrow$ i dati vanno codificati in modo da minimizzare le interferenze e rendere preciso il movimento della testina su ogni singolo bit: nella \textbf{codifica Manchester} (o di fase), ogni bit è codificato come una transizione di polarizzazione tra due bit (la transizione da valore basso a valore alto corrisponde a 0, e viceversa).

La ricerca del settore avviene da parte della testina iniziando a leggere la traccia, nella quale vi sono accanto alle informazioni delle intestazioni che specificano il settore corrente. L'inizio di un settore è codificato da una certa sequenza di byte specifici, in modo che la testina possa sincronizzarsi con l'inizio del settore e identificare nella sequenza di bit dove iniziano e finiscono i byte. Ogni settore è costituito da due \textbf{campi}, ID e dati, separati da zone della traccia, dette \textbf{gap}, in cui sono codificati i caratteri di sincronizzazione. Vi sono anche dei controlli di parità (CRC) per verificare la correttezza delle informazioni, e bit di informazione sulla traccia, sulla faccia e sul settore.

Il \textbf{disk controller} deve avere una circuiteria e una memoria (\textbf{data buffer}) per controllare le varie parti del disco, per il confronto con il CRC e per la decodifica dei dati. Il disk controller comunica con il processore attraverso un'interfaccia IDE, SATA, ecc.

Nel bootstrap, prima del primo accesso al disco fisso viene letta una ROM per la inizializzazione del sistema.

\subsection[RAID]{RAID\footnote{Per approfondire, si veda la voce \href{https://it.wikipedia.org/wiki/RAID}{RAID} su Wikipedia in italiano.}}
Il \textbf{RAID} è una proposta di standard che serve per:
\begin{itemize}
\item rendere l'insieme fisico di più dischi equivalente a un unico disco virtuale, con parti hardware e software che gestiscono le richieste in maniera trasparente;
\item migliorare l'affidabilità;
\end{itemize}
e può anche fornire qualche vantaggio in termini di prestazioni.

È possibile scegliere tra 6 configurazioni dette \textbf{livelli}: RAID 0, RAID 1, RAID 2, RAID 3, RAID 4, RAID 5.

\subsubsection{RAID 0}
\begin{figure}
	\centering
	\includegraphics[width=0.18\linewidth]{pic/09/RAID_0}
	\caption[]{RAID 0.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:RAID_0.svg}{RAID 0.svg}), è stata realizzata da \href{https://en.wikipedia.org/wiki/User:Cburnett}{Colin M. L. Burnett} ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

\noindent
Nel \textbf{RAID 0}, i dati vengono distribuiti su \textbf{strisce} (strip) di dimensione variabile (dell'ordine di qualche byte), ciascuna stante su un disco. L'Array Management Software si interpone tra l'utente e i dischi fisici, che non sono legati in modo particolare tra loro.

\paragraph{Vantaggio} la banda in lettura/scrittura è maggiore perché le operazioni di lettura/scrittura vengono effettuate in parallelo sui vari dischi
\FloatBarrier

\subsubsection{RAID 1}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[width=0.18\linewidth]{pic/09/RAID_1}
	\caption[]{RAID 1.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:RAID_1.svg}{RAID 1.svg}), è stata realizzata da \href{https://en.wikipedia.org/wiki/User:Cburnett}{Colin M. L. Burnett} ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

Anche nel \textbf{RAID 1} i dati sono distribuiti su strisce. I dischi sono suddivisi in due gruppi che sono in \textbf{mirroring} tra loro, cioè le informazioni sono replicate su entrambi i gruppi.

\paragraph{Vantaggio} affidabile
\paragraph{Svantaggio} lo spazio disponibile è dimezzato

\subsubsection{RAID 2}
Il \textbf{RAID 2} non è molto utilizzato perché è complicato: le strisce si riducono a singoli byte e sono lette in modo sincronizzato, il gestore è di tipo hardware, e sono riservati alcuni dischi ai codici di Hamming\footnote{Si veda la sezione~\ref{sez:memorie_ram_dinamiche}.}.

\subsubsection{RAID 3 e RAID 4}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/09/RAID_4}
	\caption[]{RAID 4.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:RAID_4.svg}{RAID 4.svg}), è stata realizzata da \href{https://en.wikipedia.org/wiki/User:Cburnett}{Colin M. L. Burnett} ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

\noindent
Il \textbf{RAID 3} e il \textbf{RAID 4} prevedono un solo disco riservato alle informazioni di parità. Il bit di parità non informa in quale disco si è verificato l'errore, ma se il sistemista riesce a sapere in quale disco si è verificato l'errore può utilizzare il disco di parità per ricostruire completamente le informazioni memorizzate in quel disco. Nel RAID 3 le strisce si riducono a singoli byte.

\paragraph{Svantaggio} le operazioni di lettura e scrittura in parallelo sono limitate dalle prestazioni del disco di parità, che potrebbe diventare un collo di bottiglia
\FloatBarrier

\subsubsection{RAID 5}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/09/RAID_5}
	\caption[]{RAID 5.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:RAID_5.svg}{RAID 5.svg}), è stata realizzata da \href{https://en.wikipedia.org/wiki/User:Cburnett}{Colin M. L. Burnett} ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

\noindent
Il \textbf{RAID 5} distribuisce le informazioni di parità un po' su un disco un po' su un altro. 

\paragraph{Vantaggio} si evitano colli di bottiglia
\FloatBarrier

\section{Memorie a nastro}
Nelle \textbf{memorie a nastro}, le informazioni sono memorizzate su tipicamente 9 tracce, di cui l'ultima contenente le informazioni di parità; la testina legge serialmente il nastro che si avvolge. Ogni traccia è organizzata in record, distanziati da gap per la sincronizzazione della lettura.

\section{Memorie ottiche}
Le \textbf{memorie ottiche} hanno una maggiore affidabilità: i guasti transitori sono più rari e la durata nel tempo è maggiore.

I dischi ottici hanno un'organizzazione \textbf{Constant Linear Velocity} (CLV): il motore ruota a velocità lineare costante, ma a velocità angolari diverse a seconda della posizione del raggio laser (che prende il posto della testina) $\Rightarrow$ il controllore è più complicato.