\chapter{Architettura 8086}
Il processore Intel 8086 è caratterizzato da:
\begin{itemize}
\item \textbf{spazio di indirizzamento:} la quantità di memoria che è in grado di indirizzare ($2^{20}$ indirizzi da 1 byte = 1 MB);
\item \textbf{parallelismo:} le lunghezze di parole con cui i moduli sono in grado di operare (16 bit).
\end{itemize}

La memoria è suddivisa in due porzioni, definite per dimensioni e posizione: una contiene i dati del programma, l'altra contiene il \textbf{codice macchina} del programma.

\section{Registri}
L'\textbf{instruction set architecture} (ISA) è l'insieme delle informazioni pubblicate dal produttore del processore.

\subsection{Classificazione}
\begin{itemize}
\item \textbf{special register:} (es. PC, IR) non possono essere acceduti dal programmatore in modo diretto, anche se ad esempio l'istruzione \texttt{JMP} accede indirettamente al PC;
\item \textbf{user register:} (es. AX, DI) possono essere usati dal programmatore in modo esplicito nelle istruzioni.
\end{itemize}

\hrule

\begin{itemize}
\item \textbf{registri di dato:} (AX, BX, CX, DX) sono 4 registri da 16 bit, ciascuno dei quali è costituito da una coppia di registri da 8 bit (H e L);
\item \textbf{registri indice:} (es. DI, BX) oltre a poter contenere i dati, possono essere usati anche per passare un indirizzo (es. \texttt{vett[DI]});
\item \textbf{registri contatore} (es. CX);
\item \textbf{registri di segmento} (Code Segment [CS], Data Segment [DS], Stack Segment [SS], Extra Data Segment [ES]);\footnote{Si veda la sezione~\ref{sez:segment_address}.}
\item \textbf{registri puntatore} (es. Instruction Pointer [IP], Stack Pointer [SP], Base Pointer [BP]).\footnote{Si veda la sezione~\ref{sez:effective_address}.}
\end{itemize}

Ci possono anche essere dei registri di servizio, che il progettista ha destinato solo all'uso interno.

\subsection{Status register}
Lo \textbf{status register} è un modulo da 16 bit, 9 dei quali contengono i flag di condizione e di controllo, che vengono letti continuamente tra un'istruzione e l'altra:
\begin{itemize}
\item \textbf{flag di controllo:} definiscono alcune modalità di comportamento del processore (per es. l'Interrupt Flag [IF] acceca il processore ai segnali di interrupt);
\item \textbf{flag di condizione:} il processore deve poter valutare le condizioni delle istruzioni \texttt{if} e saltare alle \texttt{sub} appropriate $\Rightarrow$ a differenza dell'istruzione \texttt{JMP}, che è un'istruzione di salto incondizionato che ne scrive l'indirizzo nel PC senza valutare alcuna condizione di istruzioni \texttt{if}, le istruzioni di salto condizionato \mbox{\texttt{JN\textless nome\_flag\textgreater}}\ decidono se scrivere o no l'indirizzo nel PC a seconda di certi flag di condizione \mbox{\texttt{\textless nome\_flag\textgreater F}}, che accompagnano l'uscita della ALU al termine di un'operazione.
\end{itemize}

\section{Accesso alla memoria}
\label{sez:accesso_memoria}
Il BHE è un segnale di enable che, nel caso di indirizzi di memoria pari, specifica se la memoria deve restituire una coppia di byte, cioè la word intera (BHE = 0), oppure un singolo byte (BHE = 1):

\begin{table}[H]
\centering
\centerline{\begin{tabular}{|c|c|c|}
\hline
\textbf{A\textsubscript{0} (bit meno significativo)} & \textbf{BHE} & \textbf{Byte restituiti} \\
\hline
\multirow{2}{*}{0 (pari)} & 0 & 2 \\
\cline{2-3}
& 1 & 1 \\
\hline
\multirow{2}{*}{1 (dispari)} & 0 & 1 \\
\cline{2-3}
& 1 & don't care\footnotemark\\
\hline
\end{tabular}}
\end{table}
\footnotetext{Si veda la sezione~\ref{sez:dont_care}.}

Un numero su 2 byte viene memorizzato in memoria con la rappresentazione little endian, ovvero il byte meno significativo precede quello più significativo.

Alcune parti della memoria predefinite sono \textbf{riservate} per esempio al bootstrap (Reset Bootstrap) e alla gestione degli interrupt (Interrupt Vector Table).

Ogni istruzione in codice macchina richiede da 1 a 6 byte, e comprende il \textbf{codice operativo} che identifica il tipo di operazione, e per ogni operando alcuni bit per identificarne il tipo (es. registro dati) e altri per il suo indirizzo.

\subsection{Indirizzi di memoria}
Il processore raggiunge ogni cella di memoria tramite un \textbf{indirizzo fisico} su 20 bit, che è la somma binaria di due sotto-indirizzi: il segment address e l'effective address. Questo meccanismo impedisce che un programma esca dai segmenti di memoria ad esso riservati, anche se le operazioni sugli indirizzi richiedono un certo tempo.

\subsubsection{Segment address}
\label{sez:segment_address}
Il \textbf{segment address} è l'indirizzo di partenza del segmento che contiene la cella di memoria.

La memoria dell'8086, di dimensioni totali pari a 2\textsuperscript{20} byte, è suddivisa in \textbf{paragrafi} da 16 byte $\Rightarrow$ i 4 bit meno significativi dell'indirizzo di partenza di ogni paragrafo risultano sempre uguali a 0 $\Rightarrow$ bastano 16 bit per identificare un paragrafo. Siccome la posizione iniziale di un paragrafo coincide anche con la posizione iniziale di un segmento da 64 KB = 2\textsuperscript{16} byte, il segment address ha una lunghezza pari a 16 bit anziché 20 bit.

I primi 16 bit dei segment address, tipicamente relativi ai segmenti di memoria codice (CS), dati (DS) e stack (SS), sono memorizzati nei \textbf{registri di segmento}. L'ES è usato per i segmenti di memoria maggiori di 64 KB.

\subsubsection{Effective address}
\label{sez:effective_address}
L'\textbf{effective address} è l'offset della cella di memoria rispetto all'indirizzo di partenza del segmento.

L'effective address richiede 16 bit perché ogni segmento ha dimensione pari a 64 KB.

I \textbf{registri puntatore} Instruction Pointer (IP), Stack Pointer (SP) e Base Pointer (BP) memorizzano gli effective address correnti, da sommare agli opportuni segment address. Anche i registri indice SI e DI possono essere usati come registri puntatore.

\subsection{Segmento di stack}
\label{sez:segmento_stack}
Nella memoria, a ogni programma è assegnato, oltre ai segmenti dati e codice, un terzo segmento di memoria detto \textbf{stack}, che implementa una coda LIFO. Nello stack, il processore non opera direttamente sugli indirizzi di memoria, ma compie solo delle operazioni di push (inserimento) e pop (estrazione).

Il registro \textbf{Stack Segment} (SS) contiene il segment address, cioè l'indirizzo della testa dello stack. Lo stack si riempie a partire dalla coda verso la testa. Lo \textbf{Stack Pointer} (SP) è uno special register, invisibile al programmatore, che memorizza l'effective address, cioè l'indirizzo della prima cella correntemente libera dello stack; viene decrementato e incrementato di 2 byte rispettivamente dalle operazioni di push e pop. Il processore non si accorge se lo stack è vuoto $\Rightarrow$ esiste il rischio di eccesso di operazioni di pop.